package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.service.TradesService;
import com.citi.training.trades.model.Trades;


@RestController
@RequestMapping("/trades")
public class TradesController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradesController.class);
	
	@Autowired
    private TradesService tradeService;
	
	/**
     * Find all {@link com.citi.training.trades.model.Trades}'s
     * @return {@link com.citi.training.trades.model.Trades}'s that were found or HTTP 404.
     */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Trades> findAll(){
        LOG.debug("findAll() was called");
        return tradeService.findAll();
    }

    /**
     * Find an {@link com.citi.training.trades.model.Trades} by its integer id.
     * @param id
     * @return {@link com.citi.training.trades.model.Trades} that was found or HTTP 404.
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Trades findById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        return tradeService.findById(id);
    }

    /**
     * Create an {@link com.citi.training.trades.model.Trades} 
     * @param employee
     * @return {@link com.citi.training.trades.model.Trades} that was created or HTTP 404.
     */
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trades> create(@RequestBody Trades trade) {
        LOG.debug("create was called, trade: " + trade);
        return new ResponseEntity<Trades>(tradeService.create(trade),
                                            HttpStatus.CREATED);
    }

    /**
     * Delete an {@link com.citi.training.trades.model.Trades} 
     * @param id
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        tradeService.deleteById(id);
    }
	
	

}
