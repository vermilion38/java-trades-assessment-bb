package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.model.Trades;

@Component
public class MysqlTradesDao implements TradesDao{
	
	@Autowired
    JdbcTemplate tpl;
	
	//create, retrieve and delete rows from the trade table.
	
	 /**
     * Create an {@link com.citi.training.trades.model.Trades} in the database
     */
    public Trades create(Trades trade) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        tpl.update(
           new PreparedStatementCreator() {
               @Override
               public PreparedStatement createPreparedStatement(Connection connection)
                                                                   throws SQLException {

                   PreparedStatement ps =
                           connection.prepareStatement(
                                   "insert into trade (stock, price, volume) values (?, ?,?)",
                           Statement.RETURN_GENERATED_KEYS);
                   ps.setString(1,  trade.getStock());
                   ps.setDouble(2,  trade.getPrice());
                   ps.setInt(3, trade.getVolume());
                   return ps;

               }
           },
           keyHolder
         );
        trade.setId(keyHolder.getKey().intValue());
        return trade;
    }
    
    /**
     * Delete an {@link com.citi.training.trades.model.Trades} from the database
     */
    public void deleteById(int id) {
        findById(id);
        tpl.update("DELETE FROM trades WHERE id=?", id);
    }
    
    /**
     * Find an {@link com.citi.training.trades.model.Trades} in the database
     */
    public Trades findById(int id) {
        List<Trades> trades = tpl.query(
                "SELECT id, stock, price,volume FROM trades WHERE id=?",
                new Object[] {id},
                new TradesMapper()
        );
        
        if(trades.size() <= 0) {
            System.out.println("Trade not found");
        }
        
        return trades.get(0);
        
    }
    
    /**
     * Find all {@link com.citi.training.trades.model.Trades}'s from the database
     */
    public List<Trades> findAll() {
        return tpl.query("SELECT id, stock, price, volume FROM trade",
                         new TradesMapper());
    }
    
    private static final class TradesMapper implements RowMapper<Trades>{

        public Trades mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Trades(rs.getInt("id"),
            					rs.getString("stock"),
                                rs.getDouble("price"),
                                rs.getInt("volume"));
        }

    }
	
	
	

}
