package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trades;

public interface TradesDao {
	
	Trades findById(int id);
	
	List<Trades> findAll();
	
	void deleteById(int id);
	
	Trades create(Trades trade);

}
