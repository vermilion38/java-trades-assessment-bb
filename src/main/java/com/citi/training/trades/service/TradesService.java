package com.citi.training.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradesDao;
import com.citi.training.trades.model.Trades;

@Component
public class TradesService {
	
	@Autowired
	private TradesDao tradesDao;
	
	public Trades findById(int id) {
		return tradesDao.findById(id);		
	}
	
	public List<Trades> findAll() {
		return tradesDao.findAll();		
	}
	
	public void deleteById(int id) {
		tradesDao.deleteById(id);
	}
	
	public Trades create(Trades trade) {
		if(trade.getStock().length()>0) {
    		return tradesDao.create(trade);
    	}
    	throw new RuntimeException("Invalid parameter:Trade stock: "+ trade.getStock());		
	}

}
