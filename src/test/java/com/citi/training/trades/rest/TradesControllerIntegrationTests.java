package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trades.model.Trades;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradesControllerIntegrationTests {

    private static final Logger logger = LoggerFactory.getLogger(TradesControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getTrades_returnsTrades() {
        restTemplate.postForEntity("/trades", new Trades(-1, "Joe", 123.23,200), Trades.class);

        ResponseEntity<List<Trades>> getAllResponse = restTemplate.exchange(
                "/employees", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Trades>>() {});

        logger.info("getAllTrades response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getStock().equals("Joe"));
        assertEquals(getAllResponse.getBody().get(0).getPrice(), 123.23, 0.0001);
        assertEquals(getAllResponse.getBody().get(0).getVolume(), 200);

        // It's not easy to rollback the transactions here as we're interfacing to a web server
        // instead, we'll delete the employee we created.
        restTemplate.delete("/trades/" + getAllResponse.getBody().get(0).getId());
    }

    @Test
    public void getTrades_returnsNotFound() {

        ResponseEntity<Trades> getByIdResponse = restTemplate.exchange(
                "/employees/9999", HttpMethod.GET, null,
                new ParameterizedTypeReference<Trades>() {});

        logger.info("getByIdTrades response: " + getByIdResponse.getBody());

        assertEquals(HttpStatus.NOT_FOUND, getByIdResponse.getStatusCode());
    }
}
