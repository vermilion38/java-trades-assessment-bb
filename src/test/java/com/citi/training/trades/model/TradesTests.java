package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.trades.model.Trades;

public class TradesTests {
	
	private int testId = 66;
	private String testStock= "AAPL";
	private double testPrice = 3200.00;
	private int testVolume= 200;
	
	@Test
	public void test_Trades_constructor() {
		Trades testTrade = new Trades(testId, testStock, testPrice, testVolume);
		
		assertEquals(testId, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice,testTrade.getPrice(), 0);
		assertEquals(testVolume,testTrade.getVolume());
		
	}
	
	@Test
	public void test_Trades_toString() {
		String testString = new Trades(testId,testStock, testPrice,testVolume).toString();
		
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(testStock));
		assertTrue(testString.contains((new Double(testPrice)).toString()));
		assertTrue(testString.contains((new Integer(testVolume)).toString()));
		
		
	}
	

}
